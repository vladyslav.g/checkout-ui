import React, {useState, useEffect} from 'react';
import {
    render,
    Banner,
    useSettings,
    useCartLines,
    useApplyCartLinesChange
} from '@shopify/checkout-ui-extensions-react';

render('Checkout::Dynamic::Render', () => <App/>);

async function App() {
    const {isUpsellByRelatedProducts, upsellVariantId, relatedProductsIds, successMessage} = useSettings();
    const cartLines = useCartLines();
    const applyCartLinesChange = useApplyCartLinesChange();
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const hasUpsellInOrder = cartLines.some(item => item.merchandise.id === upsellVariantId);
    const isAddUpsell = isUpsellByRelatedProducts ? hasRelatedProductsInOrder() : true;

    useEffect(() => {
        if (showSuccessMessage) {
            const timer = setTimeout(() => {
                setShowSuccessMessage(false)
            }, 5000);

            return () => clearTimeout(timer);
        }
    }, [showSuccessMessage]);

    function hasRelatedProductsInOrder() {
        if (!relatedProductsIds) return false;

        const relatedProductsIdsObject = relatedProductsIds
            .split(',')
            .reduce((object, productId) => {
                object[productId] = true;

                return object;
            }, {});

        return cartLines
            .map(item => {
                const itemIdSegment = item.merchandise.product.id.match(/\/([^/]+)$/);
                return itemIdSegment ? itemIdSegment[1] : null;
            })
            .some(itemId => relatedProductsIdsObject[itemId]);
    }

    async function addUpsell() {
        try {
            const res = await applyCartLinesChange({
                type: 'addCartLine',
                merchandiseId: upsellVariantId,
                quantity: 1,
            });

            return res.type === 'success'
                ? setShowSuccessMessage(true)
                : console.error(res.message);
        } catch (error) {
            console.error('An error occurred: ', error);
        }
    }

    if (upsellVariantId && !hasUpsellInOrder && isAddUpsell) await addUpsell();

    return showSuccessMessage && (
        <Banner status='success'>
            {successMessage ?? 'Upsell product was added to the order'}
        </Banner>
    );
}