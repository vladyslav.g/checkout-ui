import React from 'react';
import {
    render,
    useSettings,
    useShippingAddress,
    Link,
    Modal,
    TextBlock,
} from '@shopify/checkout-ui-extensions-react';

render('Checkout::Dynamic::Render', () => <App/>);

function App() {
    const {link_text, modal_text, target_province_codes} = useSettings();
    const {provinceCode} = useShippingAddress();
    const isProvinceMatched = provinceCode && target_province_codes
        ?.split(",")
        .map(item => item.toLowerCase().replace(/\s/g, ''))
        .includes(provinceCode.toLowerCase());

    const renderModalText = () => {
        return modal_text.replace(`{{province}}`, provinceCode);
    }

    if (!isProvinceMatched || !link_text || !modal_text) return null;

    return (
        <Link
            overlay={
                <Modal padding>
                    <TextBlock>
                        {renderModalText()}
                    </TextBlock>
                </Modal>
            }
        >
            {link_text}
        </Link>
    );
}